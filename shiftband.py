K = 5
import random
from math import exp,sqrt,log
import numpy


def x (i,t) :
    Kf = float (K)
    if i == 1 : 
        if t < 25000 : return numpy.random.normal (0.8,0.1)
        return numpy.random.normal (0.2,0.1)
    if i == 2 :
        if t < 35000 : return numpy.random.normal (0.6,0.1)
        return numpy.random.normal (0.1,0.05)
    if i == 3 :
        if t < 15000 : return numpy.random.normal (0.9,0.05)
        return numpy.random.normal (0.1,0.05)
    if i == 4 :
        if t < 20000 : return numpy.random.normal (0.4,0.1)
        return numpy.random.normal (0.3,0.1)
    if i == 0 : 
        if t < 10000 : return numpy.random.normal (0.3,0.1)
        return numpy.random.normal (0.4,0.1)
    return 0


def tirage (l) :
    a = random.random()
    L = len(l)
    S = l[0]
    h = 0
    while a > S :
        h+=1
        S+=l[h]
    return h



def shiftband (a,b,n,g,T,S0) :
    w = [1]*K
    R = 0
    y2 = []
    for t in range (T) :
        W = sum (w)
        p = [((1-g)*w[i]/W + g/K) for i in range (K)] 
        j = tirage (p)
        R += x(j,t)
        for i in range (K) :
            x1 = [0]*K
            x1[j] = x(j,t)/p[j]
        for i in range (K) :
            w[i] = w[i]*exp(n*(x1[i] + a/(p[i]*sqrt(T*K/S0)))) + b*W/K
        y2.append (R)
    return y2


T = 50000
delta = 0.1
a = 2 * sqrt (log(T*T*T*K/delta))
b = 1 / T
S0 = 1
n = sqrt(log(T*K)*S0/(T*K))
g = 2*K*n

## print (shiftband(a,b,n,g,T,S0))

def max_rec() :
    S = 0
    y = []
    for t in range (T) :
        r = 0
        for i in range (K) :
            r = max (r,x(i,t))
        S+=r
        y.append(S)
    return y

y1 = max_rec ()

def aleatoire (T) :
    y3 = []
    S = 0
    for t in range (T) :
        S+= x(random.randint(0,K-1),t)
        y3.append(S)
    return y3

import numpy as np
import matplotlib.pyplot as plt

x1 = [i for i in range (T)]

##plt.plot (x1[39950:40500],y1[39950:40500])
plt.plot (x1[39950:40500],shiftband(a,b,n,g,T,S0)[39950:40500])
##plt.plot (x1[39950:40500],aleatoire (T)[39950:40500])

print (y1[-1])

plt.show ()

plt.plot (x1,y1,label="recompense maximale")
plt.plot (x1,shiftband(a,b,n,g,T,S0),label="shiftband")
plt.plot (x1,aleatoire (T),label="au hasard")


plt.show ()

import numpy

def shiftband_b (a,b,n,g,T,S0) :
    w = [1]*K
    R = 0
    y2 = []
    for t in range (T) :
        W = sum (w)
        p = [((1-g)*w[i]/W + g/K) for i in range (K)] 
        j = tirage (p)
        if t%100 == 0 : y2.append (j)
        else : y2.append (y2[t-1])
        for i in range (K) :
            x1 = [0]*K
            x1[j] = x(j,t)/p[j]
        for i in range (K) :
            w[i] = w[i]*exp(n*(x1[i] + a/(p[i]*sqrt(T*K/S0)))) + b*W/K
    return y2

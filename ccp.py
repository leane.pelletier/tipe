from random import randint
from math import log, sqrt
import numpy as np
import matplotlib.pyplot as plt


def pioche(n):
    return randint(1, n)


def mini(l):
    m = l[0]
    for i in l:
        if i < m:
            m = i
    return m


def est_nouvelle(c, v):
    if c[v-1] == 0:
        return True
    return False


def nb_collecte(n):
    c = [0]*(n)
    i = 0
    nb = 0
    while nb < n:
        v = pioche(n)
        if est_nouvelle(c, v):
            nb += 1
        c[v-1] += 1
        i += 1
    return i


def esp(n):
    s = 0
    for i in range(1, n+1):
        s += n/float(i)
    return s


def var(n):
    s = 0
    for i in range(1, n+1):
        s += 1/float(i*i)
    return n*n*s


k = 1
N = 500

Y1 = [0]*(N-1)
Y2 = []
Y3 = []
V1, V2 = [], []
X = []

for i in range(1, N):
    X.append(i)
    Y2.append(i*log(i))
    Y3.append(esp(i))
    mu = sqrt(var(i))
    V1.append(Y3[i-1] - mu)
    V2.append(Y3[i-1] + mu)
    print(i)

for j in range(k):
    print(j)
    for i in range(N-1):
        Y1[i] += nb_collecte(i)/float(k)


plt.plot(X, Y1, label="Simulation")
#plt.plot (X,Y2, label = "lim")
plt.plot(X, Y3, label="E(Xk)")
plt.plot(X, V1)
plt.plot(X, V2)
plt.ylabel("Xk nombre d'achats")
plt.xlabel("k nombre d'emplacements dans l'album")
plt.legend()
plt.show()


def echanges(L, nb, q):
    for i in range(q):
        for k in range(25):
            if L[i][k] == 0:
                b = True
                for j in range(i+1, q):
                    if L[j][k] > 1:
                        for l in range(25):
                            if L[j][l] == 0 and L[i][l] > 1 and b:
                                L[i][k] += 1
                                L[j][l] += 1
                                L[j][k] -= 1
                                L[i][l] -= 1
                                nb[i] += 1
                                nb[j] += 1
                                b = False


def incomplete(l, q):
    for i in range(q):
        if l[i] < 25:
            return True
    return False


def collecte2(q):
    F = []
    for i in range(q):
        F.append([0]*25)
    nb = [0]*q
    nb_v = 0
    while incomplete(nb, q):
        for i in range(q):
            if nb[i] < 25:
                c = pioche(25)
                if F[i][c] == 0:
                    nb[i] += 1
                F[i][c] += 1
                nb_v += 1
        echanges(F, nb, q)
    return nb_v


X = []
N = 1000
for q in range(1, 20):
    m = 0
    for k in range(N):
        n = collecte2(q)/float(q)
        m += n/float(N)
        print(k, q)
    X.append(m)

Y = []
for i in range(len(X)):
    for k in range(int(X[i])):
        Y.append(i)

plt.hist(Y, range=(1, 20), bins=20, align="mid", rwidth=0.9, color="b")
plt.xlabel("NOMBRE DE COLLECTIONNEURS")
plt.ylabel(
    "Nombre moyen d'achats par collectionneur (moyenne sur 1000 simulations)")
plt.show()

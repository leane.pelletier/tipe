from math import exp
import numpy as np
import matplotlib.pyplot as plt

def integrale(f, a, b, n):
    somme = 0
    h = float(b-a) / float(n)
    x = a
    for i in range(0, n + 1):
        somme += f(x) * h
        x += h
    return somme

def f (x) :
    return 1-(1-exp(-x/6.0))*((1-exp(-x/12.0))**7)*((1-exp(-x/24.0))**4)*((1-exp(-x/36.0))**2)*(((1-exp(-x/72.0))**2))

print (integrale(f,0,10000,1000000))

"""resutlat : il faut 123.74 boites pour que je complete ma collection de mystery minies"""

L = [0]*12 + [1]*6 + [2]*6 + [3]*6 + [4]*6 + [5]*6 + [6]*6 + [7]*6 + [8] * 3 + [9] * 3 + [10] * 3 + [11] * 3 + [12] * 2 + [13] * 2 +[14,15]
L0 = [i for i in range (16)]

from random import choice

def pioche () :
    return choice (L)

def est_nouvelle (c,v) :
    if c[v] == 0 : return True
    return False

def collecte () :
    c = [0]*16
    i = 0
    nb = 0
    while nb < 16 :
        v = pioche ()
        if est_nouvelle (c,v) : nb += 1
        c[v] += 1
        i += 1
    return i

l = [collecte() for i in range (100000)]
print(l)

s = 0 
for i in range (100000) :
    s += l[i] / 100000.0

print (s)


Y = []
m = 0

for i in range (10000) :
    c = collecte()
    Y.append (c)
    m+= c/10000.0

print (m)
plt.hist (Y, range = (0,700), bins=100,align="mid",rwidth=0.9,color="b")
plt.xlabel("Xk")

plt.show()

"""on trouve  123.37 ou 123.83 etc..."""


def echanges (L,nb,q) :
    for i in range (q) :
        for k in range (16) :
            if L[i][k] == 0 :
                b = True
                for j in range (i+1,q) :
                    if L[j][k] > 1 :
                        for l in range (16) :
                            if L[j][l] == 0 and L[i][l] > 1 and b :
                                L[i][k] += 1
                                L[j][l] += 1
                                L[j][k] -= 1
                                L[i][l] -= 1
                                nb[i] += 1
                                nb[j] += 1
                                b = False
    

def incomplete (l,q) :
    for i in range (q) :
        if l[i] < 16 : return True
    return False


def collecte2(q) :
    F = []
    for i in range (q) :
        F.append ([0]*16)
    nb = [0]*q
    nb_v = 0
    while incomplete (nb,q)  : 
        for i in range(q) :
            if nb[i]<16 :
                c = pioche ()
                if F[i][c] == 0 : nb[i] += 1
                F[i][c] += 1
                nb_v += 1
        echanges (F,nb,q)
    return nb_v

X = []
N = 500
for q in range (1,21) :
    m = 0
    for k in range (N) :
        n = collecte2(q)/float(q)
        m += n/float(N)
        print(k,q)
    X.append (m)

Y = []
for i in range (len(X)) :
    for k in range (int(X[i])) :
        Y.append (i)
inter = [0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5,10.5,11.5,12.5,13.5,14.5,15.5,16.5,17.5,18.5,19.5]
plt.hist (Y, bins=inter, rwidth=0.8)
plt.xlabel("Nombre de collectionneurs")
plt.ylabel ("Nombre moyen d'achats par collectionneur (moyenne sur 1000 simulations)")
plt.title ("Figurines mysteres")
plt.show ()

